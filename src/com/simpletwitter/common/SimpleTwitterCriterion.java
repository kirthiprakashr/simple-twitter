package com.simpletwitter.common;

import java.util.ArrayList;
import java.util.List;

public class SimpleTwitterCriterion {

	public enum TYPE {
		LOGICAL, EXPRESSION, ORDER, PROJECTION
	}

	public enum OPERATION {
		EQ, NE, GT, LT, LE, GE, BETWEEN, OR, AND, NOTNULL, NULL
	}

	private Object value;
	private String lhs;
	private String rhs;
	private Object minValue;
	private Object maxValue;
	private String alias;

	private OPERATION operation;
	private TYPE type;

	private List<SimpleTwitterCriterion> criterionList = new ArrayList<SimpleTwitterCriterion>();

	private static SimpleTwitterCriterion exp(String field, Object value,
			OPERATION oper) {
		SimpleTwitterCriterion criterion = new SimpleTwitterCriterion();
		criterion.lhs = field;
		criterion.value = value;
		criterion.operation = oper;
		criterion.type = TYPE.EXPRESSION;

		return criterion;
	}

	public static SimpleTwitterCriterion eq(String field, Object value) {
		return exp(field, value, OPERATION.EQ);
	}

	public static SimpleTwitterCriterion ne(String field, Object value) {
		return exp(field, value, OPERATION.NE);
	}

	public static SimpleTwitterCriterion or(
			SimpleTwitterCriterion... optCriterion) {
		return logical(OPERATION.OR, optCriterion);
	}

	public static SimpleTwitterCriterion and(
			SimpleTwitterCriterion... optCriterion) {
		return logical(OPERATION.AND, optCriterion);
	}

	private static SimpleTwitterCriterion logical(OPERATION oper,
			SimpleTwitterCriterion... optCriterion) {
		SimpleTwitterCriterion criterion = new SimpleTwitterCriterion();

		criterion.type = TYPE.LOGICAL;
		criterion.operation = oper;

		for (SimpleTwitterCriterion attriboCriterion : optCriterion) {
			criterion.criterionList.add(attriboCriterion);
		}

		return criterion;
	}

	public boolean hasCriterionList() {
		if (criterionList != null && criterionList.size() > 1)
			return true;
		else
			return false;
	}

	public List<SimpleTwitterCriterion> getCriterionList() {
		return criterionList;
	}

	public OPERATION getOperation() {
		return operation;
	}

	public String getField() {
		return lhs;
	}

	public Object getValue() {
		return value;
	}

	public TYPE getType() {
		return type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

}

package com.simpletwitter.web.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.simpletwitter.models.Tweet;
import com.simpletwitter.models.TwitterUser;
import com.simpletwitter.service.LoginService;
import com.simpletwitter.service.TweetService;
import com.simpletwitter.service.TwitterUserService;

@Controller
@RequestMapping(value = "/twitterhomepage.html")
public class LoginOrRegisterResponseHandler {

	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(method = RequestMethod.POST, params = "bean=TwitterUser")
	public ModelAndView handleRequest(
			@ModelAttribute("TwitterUser") TwitterUser tuser,
			HttpServletRequest request) throws Exception {
		ModelAndView mv = new ModelAndView();
		TwitterUser validateUser = LoginService.validateUser(tuser);
		if (validateUser != null) {
			saveUserToSession(request, validateUser);
			forwardToHomePage(mv, validateUser);
		} else {
			loginFailed(mv, "Username password combination does not match");
		}
		return mv;
	}

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView handleGetRequest(HttpServletRequest request)
			throws Exception {
		ModelAndView mv = new ModelAndView();
		HttpSession session = request.getSession();
		TwitterUser tuser = (TwitterUser) session.getAttribute("twitteruser");
		if (tuser != null) {
			forwardToHomePage(mv, tuser);
		} else {
			loginFailed(mv, null);
		}
		return mv;
	}

	private void saveUserToSession(HttpServletRequest request,
			TwitterUser objectByExample) {
		HttpSession session = request.getSession();
		session.setAttribute("twitteruser", objectByExample);
	}

	private void forwardToHomePage(ModelAndView mv, TwitterUser objectByExample) {
		Set<TwitterUser> allUserList = TwitterUserService
				.getAllUserList(objectByExample);
		Set<TwitterUser> smallUserList = new HashSet<TwitterUser>();
		int i = 5;
		for (TwitterUser twitterUser : allUserList) {
			smallUserList.add(twitterUser);
			if (--i <= 0) {
				break;
			}
		}
		Map<String, Integer> followCount = TwitterUserService
				.getFollowCount(objectByExample.getObjId());
		Set<Tweet> tweets = TweetService.getTweetStream(
				objectByExample.getObjId(), objectByExample.getFollowing());
		int tweetCount = TwitterUserService.getTweetCount(objectByExample);
		Tweet tweet = new Tweet();
		mv.addObject("tweet", tweet);
		mv.addObject("tweets", tweets);
		mv.setViewName("twitterhomepage");
		mv.addObject("user", objectByExample);
		mv.addObject("userlist", smallUserList);
		mv.addObject("followcount", followCount);
		mv.addObject("tweetcount", tweetCount);
	}

	private void loginFailed(ModelAndView mv, String errorMsg) {
		TwitterUser tuser = new TwitterUser();
		mv.addObject("TwitterUser", tuser);
		if (errorMsg != null) {
			mv.addObject("errormsg", errorMsg);
		}
		mv.setViewName("loginorregister");

	}
}

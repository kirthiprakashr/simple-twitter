package com.simpletwitter.web.controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.istreepetge.models.IpCalendarEvent;
import com.simpletwitter.models.TwitterUser;

@Controller
@RequestMapping(value = "loginorregister.html")
public class LoginOrRegisterController {

	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(value = "/welcome.html", method = RequestMethod.GET)
	public ModelAndView handleRequest() throws Exception {
		ModelAndView mv = new ModelAndView();
		TwitterUser tuser = new TwitterUser();
		mv.addObject("TwitterUser", tuser);
		mv.setViewName("loginorregister");
		return mv;
	}
}

package com.simpletwitter.web.controllers;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.simpletwitter.common.Authenticator;
import com.simpletwitter.models.Tweet;
import com.simpletwitter.models.TwitterUser;
import com.simpletwitter.service.TweetService;
import com.simpletwitter.service.TwitterUserService;

@Controller
public class TwitterUserProfileController {
	protected final Log logger = LogFactory.getLog(getClass());

	@RequestMapping(value = "/twitteruser.html", method = RequestMethod.GET)
	public ModelAndView listAllUsers(HttpServletRequest request) {
		TwitterUser currUser = Authenticator.isValid(request);
		ModelAndView mv = new ModelAndView();
		if (currUser != null) {
			Set<TwitterUser> allUserList = TwitterUserService
					.getAllUserList(currUser);
			mv.setViewName("twitteruserprofile");
			mv.addObject("currentuser", currUser);
			mv.addObject("userlist", allUserList);
		} else {
			Authenticator.loginFailed(mv, null);
		}
		return mv;
	}

	@RequestMapping(value = "/twitteruser.html", method = RequestMethod.GET, params = "objid")
	public ModelAndView viewUser(@RequestParam(value = "objid") String objId,
			HttpServletRequest request) {
		TwitterUser currUser = Authenticator.isValid(request);
		ModelAndView mv = new ModelAndView();
		if (currUser != null) {
			TwitterUser tuser = TwitterUserService.getUser(Long.valueOf(objId));
			Set<TwitterUser> allUserList = TwitterUserService
					.getAllUserList(currUser);
			Set<Tweet> tweets = TweetService.getTweets(Long.valueOf(objId));
			mv.setViewName("twitteruserprofile");
			mv.addObject("fuser", tuser);
			mv.addObject("tweetlist", tweets);
			mv.addObject("user", currUser);
			mv.addObject("userlist", allUserList);
		} else {
			Authenticator.loginFailed(mv, null);
		}
		return mv;
	}

	@RequestMapping(value = "/follow.html", method = RequestMethod.POST, params = "bean=fuser")
	public ModelAndView handleRequest(
			@ModelAttribute("user") TwitterUser tuser,
			HttpServletRequest request) throws Exception {
		TwitterUser currUser = Authenticator.isValid(request);
		ModelAndView mv = new ModelAndView();
		if (currUser != null) {
			Long objId = tuser.getObjId();
			TwitterUser fuser = TwitterUserService.getUser(objId);
			if (!currUser.equals(fuser)) {
				Set<TwitterUser> followers = fuser.getFollowers();
				if (!followers.add(currUser)) {
					followers.remove(currUser);
				}
				TwitterUserService.updateUser(fuser);
				currUser = TwitterUserService.getUser(currUser.getObjId());
				Authenticator.saveUserToSession(request, currUser);
			}
			Set<TwitterUser> allUserList = TwitterUserService
					.getAllUserList(currUser);
			Set<Tweet> tweets = TweetService.getTweets(Long.valueOf(objId));
			mv.setViewName("twitteruserprofile");
			mv.addObject("fuser", fuser);
			mv.addObject("tweetlist", tweets);
			mv.addObject("user", currUser);
			mv.addObject("userlist", allUserList);
		} else {
			Authenticator.loginFailed(mv, null);
		}
		return mv;
	}
}

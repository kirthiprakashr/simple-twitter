package com.simpletwitter.web.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.simpletwitter.models.TwitterUser;

@Controller
@RequestMapping(value = "/twitterlogout.html")
public class Logout {
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.invalidate();
		ModelAndView mv = new ModelAndView();
		TwitterUser tuser = new TwitterUser();
		mv.addObject("TwitterUser", tuser);
		mv.setViewName("loginorregister");
		return mv;
	}
}

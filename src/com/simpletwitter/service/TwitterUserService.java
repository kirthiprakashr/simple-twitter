package com.simpletwitter.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.simpletwitter.common.DatabaseManager;
import com.simpletwitter.common.SimpleTwitterCriteria;
import com.simpletwitter.common.SimpleTwitterCriterion;
import com.simpletwitter.models.Tweet;
import com.simpletwitter.models.TwitterUser;

public class TwitterUserService {

	public static Set<TwitterUser> getAllUserList(TwitterUser currentUser) {
		Set<TwitterUser> result = null;
		if (currentUser != null) {
			DatabaseManager da = DatabaseManager.getInstance();
			SimpleTwitterCriteria criteria = SimpleTwitterCriteria.instance();
			criteria.add(SimpleTwitterCriterion.ne("objId",
					currentUser.getObjId()));
			List<TwitterUser> allObjects = da.getAllObjects(TwitterUser.class,
					criteria);
			result = new HashSet<TwitterUser>(allObjects);
		}
		return result;
	}

	public static Map<String, Integer> getFollowCount(Long objId) {
		DatabaseManager da = DatabaseManager.getInstance();
		TwitterUser tuser = da.getObject(TwitterUser.class, objId);
		int followers = tuser.getFollowers().size();
		int following = tuser.getFollowing().size();
		Map<String, Integer> followCountMap = new HashMap<String, Integer>();
		followCountMap.put("followers", followers);
		followCountMap.put("following", following);
		return followCountMap;
	}

	public static TwitterUser getUser(Long ObjId) {
		DatabaseManager da = DatabaseManager.getInstance();
		TwitterUser tuser = da.getObject(TwitterUser.class, ObjId);
		return tuser;
	}

	public static void updateUser(TwitterUser user) {
		DatabaseManager da = DatabaseManager.getInstance();
		da.updateObject(user);
	}

	public static int getTweetCount(TwitterUser user) {
		DatabaseManager da = DatabaseManager.getInstance();
		SimpleTwitterCriteria criteria = SimpleTwitterCriteria.instance();
		criteria.add(SimpleTwitterCriterion.eq("twitterUser.objId",
				user.getObjId()));
		List<Tweet> allObjects = da.getAllObjects(Tweet.class, criteria);

		return new HashSet<Tweet>(allObjects).size();
	}

}

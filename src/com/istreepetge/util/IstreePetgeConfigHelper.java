package com.istreepetge.util;

public class IstreePetgeConfigHelper {

	private String clientId;

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

}

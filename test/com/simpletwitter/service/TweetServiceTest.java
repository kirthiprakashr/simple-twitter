package com.simpletwitter.service;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.simpletwitter.models.Tweet;
import com.simpletwitter.models.TwitterUser;

public class TweetServiceTest {

	@Test
	public void test() {
		TwitterUser user = TwitterUserService.getUser(3L);
		Set<TwitterUser> following = user.getFollowing();
		Set<Tweet> tweetStream = TweetService.getTweetStream(3L, following);
		for (Tweet tweet : tweetStream) {
			System.out.println(tweet.getTwitterUser().getObjId() + "date: "
					+ tweet.getDateCreated());
		}
	}
}

package com.simpletwitter.service;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import com.simpletwitter.common.DatabaseManager;
import com.simpletwitter.common.SimpleTwitterCriteria;
import com.simpletwitter.common.SimpleTwitterCriterion;
import com.simpletwitter.models.TwitterUser;

public class TwitterUserServiceTest {

	@Test
	public void test() {
		TwitterUser t = null;
		Set<TwitterUser> allUserList = TwitterUserService.getAllUserList(t);
		for (TwitterUser twitterUser : allUserList) {
			System.out.println(twitterUser);
		}
	}

	@Test
	public void testfollowcount() {
		Map<String, Integer> followCount = TwitterUserService
				.getFollowCount(1L);
		System.out.println(followCount);
	}

	@Test
	public void testfollowrelation() {
		TwitterUser user = TwitterUserService.getUser(1L);
		TwitterUser user2 = TwitterUserService.getUser(3L);
		Set<TwitterUser> following = user2.getFollowing();
		TwitterUser fuser = TwitterUserService.getUser(7L);
		Set<TwitterUser> followers = fuser.getFollowers();
		if (!followers.add(user2)) {
			followers.remove(user2);
		}
		TwitterUserService.updateUser(fuser);
		user2 = TwitterUserService.getUser(3L);
		following = user2.getFollowing();

	}

	@Test
	public void testcriteria() {
		DatabaseManager da = DatabaseManager.getInstance();
		TwitterUser user = TwitterUserService.getUser(3L);
		SimpleTwitterCriteria criteria = SimpleTwitterCriteria.instance();
		criteria.add(SimpleTwitterCriterion.ne("objId", user.getObjId()));

		List<TwitterUser> allObjects = da.getAllObjects(TwitterUser.class,
				criteria);
		for (TwitterUser twitterUser : allObjects) {
			System.out.println(twitterUser);
		}
	}

	@Test
	public void testtweetcount() {
		int tweetCount = TwitterUserService.getTweetCount(TwitterUserService.getUser(3L));
		System.out.println(tweetCount);
	}
}

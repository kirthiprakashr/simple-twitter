package com.istreepetge.web.controllers;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

public class TestOauthResponseHandler {

	@Test
	public void test() {
		OauthResponseHandler h = new OauthResponseHandler();
		ModelAndView handleOauthResponse = h.handleOauthResponse(null, null);
		assertEquals("hello", handleOauthResponse.getViewName());
	}

}
